#include <stdio.h>
int x=100,y=10,z=1;
void main(){
  int t = -20;
  __transaction_atomic { int l1=5; int l2 = z; int l3 = l2; z = l1+l3; t=x>y; }
  printf("Bye %d %d\n", t, z);
}
