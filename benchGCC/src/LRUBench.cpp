/**
 *  Copyright (C) 2015
 *  Virginia Tech
 */


#include <stdlib.h>
#include <pthread.h>
#include <stdint.h>
#include <iostream>
#include <stdio.h>
#include "bmconfig.hpp"
#include "rand32.cpp"

/**
 *  Step 1:
 *    Include the configuration code for the harness, and the API code.
 */

/**
 *  Step 2:
 *    Declare the data type that will be stress tested via this benchmark.
 *    Also provide any functions that will be needed to manipulate the data
 *    type.  Take care to avoid unnecessary indirection.
 *
 *  NB: For the simple counter, we don't need to have an abstract data type
 */
#define ADDRESS_RANGE	10000
#define TYPE	long

namespace LRU{

TYPE **data;
TYPE **address;
TYPE **hits;

/**
 *  Step 3:
 *    Declare an instance of the data type, and provide init, test, and verify
 *    functions
 */

/*** Initialize the counter */
void
bench_init()
{
	data = (TYPE **)malloc(CFG.elements * sizeof(TYPE*));
	address = (TYPE **)malloc(CFG.elements * sizeof(TYPE*));
	hits = (TYPE **)malloc(CFG.elements * sizeof(TYPE*));
	for (unsigned int index=0; index<CFG.elements; index++){
		data[index] = (TYPE *)malloc(CFG.sets * sizeof(TYPE));
		address[index] = (TYPE *)malloc(CFG.sets * sizeof(TYPE));
		hits[index] = (TYPE *)malloc(CFG.sets * sizeof(TYPE));
		for(int i=0; i<CFG.sets; i++){
			data[index][i] = -1;
			address[index][i] = -1;
			hits[index][i] = 0;
		}
	}
}

/*** Run a bunch of increment transactions */
void
bench_test(uintptr_t, uint32_t* seed)
{
	uint32_t local_seed = *seed;
    TM_BEGIN(atomic) {
    	for(unsigned int o=0; o<CFG.ops; o++){
    		unsigned int op = rand_r_32(&local_seed) % 100;
			long addr = rand_r_32(&local_seed) % ADDRESS_RANGE;
			int index = addr%CFG.elements;
    		if(op < CFG.lookpct){
    			// lookup cache (get)
				for(int i=0; i<CFG.sets; i++){
					if(TM_COMPARE(address[index][i], TM_EQ, addr)){
						TM_INCREMENT(hits[index][i], 1l);	// increase the frequency counter
						TM_READ(data[index][i]);	// read the cache
						break;
					}
				}
    		}else{
    			long d = rand_r_32(&local_seed);
    			// update cache (set)
				long min = 10000000;
				int replacment = -1;
				bool found = false;
				for(int i=0; i<10; i++){
					if(TM_COMPARE(address[index][i], TM_EQ, addr)){
						TM_WRITE(data[index][i], d);
						found = true;
						break;
					}
					if(TM_COMPARE(hits[index][i], TM_LT, min)){
						min = TM_READ(hits[index][i]);
						replacment = i;
					}
				}
				if(!found){
					TM_WRITE(address[index][replacment], addr);
					TM_WRITE(data[index][replacment], d);
					TM_WRITE(hits[index][replacment], 1l);
				}
    		}
    	}
    } TM_END;
    *seed = local_seed;
}

/*** Ensure the final state of the benchmark satisfies all invariants */
bool
bench_verify()
{
	if(CFG.verbose)
		for(unsigned int index=0; index<CFG.elements; index++){
			for(int i=0; i<CFG.sets; i++){
				printf("%lu %p[%lu %p][%lu %p]\t", address[index][i], &address[index][i], data[index][i], &data[index][i], hits[index][i], &hits[index][i]);
			}
			printf("\n");
		}
	return true;
}

/**
 *  Step 4:
 *    Include the code that has the main() function, and the code for creating
 *    threads and calling the three above-named functions.  Don't forget to
 *    provide an arg reparser.
 */

/*** no reparsing needed */
void
bench_reparse() {
    CFG.bmname = "LRU";
}

}
