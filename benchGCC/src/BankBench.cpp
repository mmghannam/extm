/**
 *  Copyright (C) 2015
 *  Virginia Tech
 *
 */

#include <pthread.h>
#include <stdint.h>
#include <iostream>
#include <stdio.h>
#include "bmconfig.hpp"
#include "rand32.cpp"

/**
 *  Step 1:
 *    Include the configuration code for the harness, and the API code.
 */

/**
 *  Step 2:
 *    Declare the data type that will be stress tested via this benchmark.
 *    Also provide any functions that will be needed to manipulate the data
 *    type.  Take care to avoid unnecessary indirection.
 *
 *  NB: For the simple counter, we don't need to have an abstract data type
 */
#define TYPE		long
#define BALANCE		10000

namespace Bank{

TYPE *accounts;

/**
 *  Step 3:
 *    Declare an instance of the data type, and provide init, test, and verify
 *    functions
 */

/*** Initialize the counter */
void
bench_init()
{
	accounts = new TYPE[CFG.elements];
	for(unsigned int i=0; i<CFG.elements; i++)
		accounts[i] = BALANCE;
}

void foo(){
}

/*** Run a bunch of increment transactions */
void
bench_test(uintptr_t, uint32_t* seed)
{
	uint32_t local_seed = *seed;
	int elements = CFG.elements;
	int ops = CFG.ops;
    TM_BEGIN(atomic) {
    	for(unsigned int i=0; i<ops; i++){
			int from = rand_r_32(&local_seed) % elements;		// 1R 1W
			int to = rand_r_32(&local_seed) % elements;			// 1R 1W
			TYPE amount = rand_r_32(&local_seed) % BALANCE;		// 1R 1W
			if(TM_COMPARE(accounts[from], TM_GTE, amount)){		// 1R 1SR
				TM_INCREMENT(accounts[from], -1 * amount);		// 1R 1SW
				TM_INCREMENT(accounts[to], amount);				// 1R 1SW
			}
														// Total = 6R 3W 1SR 2SW
    	}
    } TM_END;

//    printf("[%lu] %d [%lu] === %lu ====> %d [%lu]\n", pthread_self(), from, accounts[from], amount, to, accounts[to]);

//    bench_verify();
    *seed = local_seed;
}

/*** Ensure the final state of the benchmark satisfies all invariants */
bool
bench_verify()
{
	TYPE total = 0;
	for(unsigned int i=0; i<CFG.elements; i++){
		total += accounts[i];
//		printf("Account %d \t(%lu$) %p\n", i, accounts[i], &accounts[i]);
	}
	std::cout << "Total \t" << total <<"$\n";
    return total == CFG.elements * BALANCE;
}

/**
 *  Step 4:
 *    Include the code that has the main() function, and the code for creating
 *    threads and calling the three above-named functions.  Don't forget to
 *    provide an arg reparser.
 */

/*** no reparsing needed */
void
bench_reparse() {
    CFG.bmname = "Bank";
}

}
