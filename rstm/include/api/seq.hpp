#ifndef SEQ_API_HPP__
#define SEQ_API_HPP__

#define TM_EQ 	==
#define TM_IEQ 	!=
#define TM_GT 	>
#define TM_GTE 	>=
#define TM_LT 	<
#define TM_LTE 	<=
#define TM_AND 	&
#define TM_OR 	|

/**
 * Code should only use these calls, not the template stuff declared above
 */
#define TM_READ(var)       			(var)
#define TM_WRITE(var, val) 			({var = val; var;})
#define TM_COMPARE(var, op, val)	(var op val)
#define TM_INCREMENT(var, val)		({var += val; var;})

/**
 *  This is the way to start a transaction
 */
#ifdef GCC_TM
#define TM_BEGIN(TYPE)              __transaction_atomic {
#else
#define TM_BEGIN(TYPE)              {
#endif

/**
 *  This is the way to commit a transaction.  Note that these macros weakly
 *  enforce lexical scoping
 */
#define TM_END                      }

/**
 *  Macro to get STM context.  This currently produces a pointer to a TxThread
 */
#define TM_GET_THREAD()
#define TM_ARG_ALONE
#define TM_ARG
#define TM_PARAM
#define TM_PARAM_ALONE

#define TM_WAIVER
#define TM_CALLABLE

#define TM_SYS_INIT()
#define TM_THREAD_INIT()
#define TM_THREAD_SHUTDOWN()
#define TM_SYS_SHUTDOWN()
#define TM_ALLOC             malloc
#define TM_FREE              free
#define TM_SET_POLICY(P)

#define TM_BEGIN_FAST_INITIALIZATION()		TM_BEGIN()
#define TM_END_FAST_INITIALIZATION()		TM_END

#ifdef GCC_TM
#define TM_GET_ALGNAME()     "gcc-tm"
#else
#define TM_GET_ALGNAME()     "direct-access"
#endif

#define TM_ALIGN(X)


#endif // API_LIBRARY_HPP__
