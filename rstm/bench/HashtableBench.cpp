/**
 *  Copyright (C) 2015
 *  Virginia Tech
 *
 * License: Modified BSD
 *          Please see the file LICENSE.RSTM for licensing information
 */

#include <stm/config.h>

#if defined(STM_CPU_SPARC)
#include <sys/types.h>
#endif

#include <stdint.h>
#include <iostream>
#include <api/api.hpp>
#include <alt-license/rand_r_32.h>
#include "bmconfig.hpp"

/**
 *  We provide the option to build the entire benchmark in a single
 *  source. The bmconfig.hpp include defines all of the important functions
 *  that are implemented in this file, and bmharness.cpp defines the
 *  execution infrastructure.
 */
#ifdef SINGLE_SOURCE_BUILD
#include "bmharness.cpp"
#endif

/**
 *  Step 1:
 *    Include the configuration code for the harness, and the API code.
 */

/**
 *  Step 2:
 *    Declare the data type that will be stress tested via this benchmark.
 *    Also provide any functions that will be needed to manipulate the data
 *    type.  Take care to avoid unnecessary indirection.
 *
 *  NB: For the simple counter, we don't need to have an abstract data type
 */

#define TYPE		long
#define VALUES_RANGE	1000

struct node {
	TYPE data;
	TYPE s;
};

class Hashtable {
	int size;
	struct node *table;

	int hash_function(TYPE k, TYPE i) {
		TYPE a;
		TYPE b;
		TYPE c;

		a = k % size;
		b = 1 + (k % ( size - 2));
		c = (a + i * b) % size;

		return c;
	}

	int hash_search(TYPE k TM_ARG) {
		TYPE j;
		int i = 0;
		while (i != size) {
			j = hash_function(k, i);
			if (TM_COMPARE(table[j].data, TM_EQ, k) &&
				TM_COMPARE(table[j].s, TM_EQ, (long)'F')) {
				return j;
			}
			i++;
		}
		return -1;
	}
public:
	Hashtable(int s) {
		if(s < 3) s = 3;
		size = s;
		table = new struct node[s];
		for (int i = 0; i < size; i++) {
			table[i].s = (long)'N';
		}
	}

	bool remove(TYPE k TM_ARG) {
		int i = hash_search(k TM_PARAM);
		if (i != -1) {
			TM_WRITE(table[i].s, (long)'D');
		} else {
			return false;
		}
		return true;
	}

	bool add(TYPE k TM_ARG) {
		TYPE j;
		int i = 0;
		while (i != size) {
			j = hash_function(k, i);
//			if (table[j].s == 'N' || table[j].s == 'D') {
			if (TM_COMPARE(table[j].s, TM_EQ, (long)'N') || TM_COMPARE(table[j].s, TM_EQ, (long)'D')) {
				TM_WRITE(table[j].data, k);
				TM_WRITE(table[j].s, (long)'F');
				return true;
			}
			if (TM_COMPARE(table[j].data, TM_EQ, k))
				return false;
			i++;
		}
		if (i == size) {
			return false;
		}
		return true;
	}

	void print() {  // initialize the table
		for (int i = 0; i < size; i++)
			std::cout << i << ":" << " [" << (char)table[i].s << "]" << table[i].data << std::endl;
	}

};

static Hashtable *hashtable;;

/**
 *  Step 3:
 *    Declare an instance of the data type, and provide init, test, and verify
 *    functions
 */

/*** Initialize the counter */
void
bench_init()
{
	hashtable = new Hashtable(CFG.elements);
}

/*** Run a bunch of increment transactions */
void
bench_test(uintptr_t, uint32_t* seed)
{
	VOLATILE uint32_t local_seed = *seed;
    TM_BEGIN(atomic) {
    	for(unsigned int i=0; i<CFG.ops; i++){
    		unsigned int op = rand_r_32(&local_seed) % 100;
    		long arg = rand_r_32(&local_seed) % VALUES_RANGE;
    		if(op < CFG.lookpct){
    			// add to hashtable
    			hashtable->add(arg TM_PARAM);
    		}else{
    			// remove from hashtable
    			hashtable->remove(arg TM_PARAM);
    		}
    	}
    } TM_END;
    *seed = local_seed;
}

/*** Ensure the final state of the benchmark satisfies all invariants */
bool
bench_verify()
{
	hashtable->print();
    return true;
}

/**
 *  Step 4:
 *    Include the code that has the main() function, and the code for creating
 *    threads and calling the three above-named functions.  Don't forget to
 *    provide an arg reparser.
 */

/*** no reparsing needed */
void
bench_reparse() {
    CFG.bmname = "Hashtable";
}
