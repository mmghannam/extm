from subprocess import call

threads = [1, 2, 4, 6, 8, 16]
times = range(1,11)
nops = [5, 10, 20, 50, 100, 1000]
ops = [5, 10, 20, 50, 100, 1000]
lookpcts = range(1,11)
with open('bank_test.sh','a') as f:
    for thread in threads:
        for time in times:
            for nop in nops:
                for op in ops:
                    for lookpct in lookpcts:
                        f.write("./BankBenchSSB64 -d {} -p {} -N {} -R {} -O {} >> test.txt\n".format(time,thread, nop, lookpct * 10, op))
                        
f.close()